
# Utv-Day-Application

## What is it
## What problem does it solve
## How to use




# Contribute

The structure of the project is based the NDC talk [Vertical Slice Architecture - Jimmy Bogard](https://www.youtube.com/watch?v=SUiWfhAhgQw)

## [MediatR](https://github.com/jbogard/MediatR)

## [AutoMapper](https://github.com/AutoMapper/AutoMapper)
[What is AutoMapper?](https://automapper.org/)
AutoMapper is a simple little library built to solve a deceptively complex problem - getting rid of code that mapped one object to another. This type of code is rather dreary and boring to write, so why not invent a tool to do it for us?


### Resurces and example code:
[ContosoUniversityDotNetCore by Jimmy Bogard](https://github.com/jbogard/ContosoUniversityDotNetCore/blob/master/ContosoUniversity/)