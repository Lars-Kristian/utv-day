
using System.Reflection;
using Application.Behavours;
using AutoMapper;
using Core.Entities;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Application
{
    public class ApplicationConfiguration
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            AssemblyScanner.FindValidatorsInAssemblyContaining<ApplicationConfiguration>().ForEach(pair =>
            {
                // RegisterValidatorsFromAssemblyContaing does this:
                services.Add(ServiceDescriptor.Transient(pair.InterfaceType, pair.ValidatorType));
                // Also register it as its concrete type as well as the interface type
                // services.Add(ServiceDescriptor.Transient(pair.ValidatorType, pair.ValidatorType));
            });

            services.AddAutoMapper(typeof(ApplicationConfiguration).GetTypeInfo().Assembly);
            services.AddMediatR(typeof(ApplicationConfiguration).GetTypeInfo().Assembly);
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(LoggingPipelineBehavior<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationPipelineBehavior<,>));
        }
    }
}