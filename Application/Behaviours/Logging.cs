
using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Application.Behavours
{

    public class LoggingPipelineBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly ILogger _logger;

        public LoggingPipelineBehavior(ILogger<LoggingPipelineBehavior<TRequest, TResponse>> logger)
        {
            _logger = logger;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            var name = $"{typeof(TRequest).FullName}";

            _logger.LogInformation($"{name}");

            try
            {
                var response = await next();

                _logger.LogInformation($"{name}:Completed -> {typeof(TResponse).Name}");

                return response;
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{name}:Error");
                throw e;
            }
        }
    }
}