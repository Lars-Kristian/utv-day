using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using MediatR;
using Database;
using Microsoft.EntityFrameworkCore;

namespace Features.Ping
{
    public class Get
    {
        public class Command : IRequest<string>
        {

        }

        public class PingHandler : IRequestHandler<Command, string>
        {
            public Task<string> Handle(Command request, CancellationToken token)
            {
                return Task.FromResult("Server is running " + DateTime.Now);
            }
        }
    }
}

