using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MediatR;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Database;
using FluentValidation;

namespace Features.Card
{
    public class GetByDay
    {
        public class Query : IRequest<List<Model>>
        {
            public Guid DayId { get; set; }
        }

        public class Validator : AbstractValidator<Query>
        {
            public Validator()
            {
                RuleFor(q => q.DayId).NotEmpty();
            }
        }

        public class Model
        {
            public Guid Id { get; set; }
            public DateTime Created { get; set; }
            public string Name { get; set; }
            public string Technology { get; set; }
            public string Slack { get; set; }
            public string Contact { get; set; }
            public string Comment { get; set; }
            public List<VoteModel> Votes { get; set; }
        }

        public class VoteModel
        {
            public Guid Id { get; set; }
            public Guid CardId { get; set; }
            public string Nickname { get; set; }
        }

        public class GetByDayHandler : IRequestHandler<Query, List<Model>>
        {
            private readonly ApplicationDbContext _dbContext;
            private readonly IConfigurationProvider _mapper;

            public GetByDayHandler(ApplicationDbContext dbContext, IConfigurationProvider mapper)
            {
                _dbContext = dbContext;
                _mapper = mapper;
            }

            public async Task<List<Model>> Handle(Query request, CancellationToken token)
            {
                var result = await _dbContext
                    .Cards
                    .Where(c => c.DayId == request.DayId)
                    .Include(c => c.Votes)
                    .ProjectTo<Model>(_mapper).ToListAsync(token);

                return result;
            }
        }
    }
}