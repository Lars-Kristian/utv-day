using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using MediatR;
using Database;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using System.Linq;
using FluentValidation;

namespace Features.Card
{
    public class CreateOrUpdate
    {
        public class Command : IRequest<Model>
        {
            public Guid Id { get; set; }
            public Guid DayId { get; set; }
            public DateTime Created { get; set; }
            public string Name { get; set; }
            public string Technology { get; set; }
            public string Slack { get; set; }
            public string Contact { get; set; }
            public string Comment { get; set; }
        }

        public class Validator : AbstractValidator<Command> {
            public Validator(){
                RuleFor(c => c.Name).NotEmpty();
            }
        }

        public class Model
        {
            public Guid Id { get; set; }
            public Guid DayId { get; set; }
            public DateTime Created { get; set; }
            public string Name { get; set; }
            public string Technology { get; set; }
            public string Slack { get; set; }
            public string Contact { get; set; }
            public string Comment { get; set; }
            public List<VoteModel> Votes { get; set; }
        }

        public class VoteModel
        {
            public Guid Id { get; set; }
            public Guid CardId { get; set; }
            public string Nickname { get; set; }
        }

        public class CreateHandler : IRequestHandler<Command, Model>
        {
            private readonly ApplicationDbContext _dbContext;
            private readonly IMapper _mapper;

            public CreateHandler(ApplicationDbContext dbContext, IMapper mapper)
            {
                _dbContext = dbContext;
                _mapper = mapper;
            }

            public async Task<Model> Handle(Command command, CancellationToken token)
            {
                if (command.Id == null || command.Id == Guid.Empty)
                {
                    var data = _mapper.Map<Core.Entities.Card>(command);
                    data.Id = Guid.NewGuid();
                    data.Created = DateTime.Now;

                    var createResult = await _dbContext.Cards.AddAsync(data, token);

                    await _dbContext.SaveChangesAsync(token);
                    return _mapper.Map<Model>(createResult.Entity);

                }
                else
                {
                    var updateResult = await _dbContext.Cards
                                        .Where(c => c.Id == command.Id)
                                        .Include(c => c.Votes)
                                        .FirstOrDefaultAsync(token);

                    if (updateResult == null)
                    {
                        throw new Exception("Card not found in database -> id: " + command.Id);
                    }

                    //TODO: Can I use automapper for this?
                    updateResult.Name = command.Name;
                    updateResult.Contact = command.Contact;
                    updateResult.Technology = command.Technology;
                    updateResult.Comment= command.Comment;
                    updateResult.Slack = command.Slack;

                    await _dbContext.SaveChangesAsync(token);

                    return _mapper.Map<Model>(updateResult);
                }
            }
        }
    }
}