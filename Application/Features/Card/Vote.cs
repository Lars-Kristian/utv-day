using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using MediatR;
using Database;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using System.Linq;
using FluentValidation;

namespace Features.Card
{
    public class Vote
    {
        public class Command : IRequest<Model>
        {
            public Guid CardId { get; set; }
            public string Nickname { get; set; }
        }

        public class Validator : AbstractValidator<Command>
        {
            public Validator()
            {
                RuleFor(c => c.CardId).NotEmpty();
                RuleFor(c => c.Nickname).NotEmpty();
            }
        }

        public class Model
        {
            public Guid CardId { get; set; }
            public Guid Id { get; set; }
            public string Nickname { get; set; }
        }

        public class CreateHandler : IRequestHandler<Command, Model>
        {
            private readonly ApplicationDbContext _dbContext;
            private readonly IMapper _mapper;

            public CreateHandler(ApplicationDbContext dbContext, IMapper mapper)
            {
                _dbContext = dbContext;
                _mapper = mapper;
            }

            public async Task<Model> Handle(Command command, CancellationToken token)
            {
                var data = _mapper.Map<Core.Entities.CardVote>(command);

                //TODO: check if vote exists

                var result = await _dbContext.CardVotes.AddAsync(data, token);
                await _dbContext.SaveChangesAsync(token);

                return _mapper.Map<Model>(result.Entity);
            }
        }
    }
}