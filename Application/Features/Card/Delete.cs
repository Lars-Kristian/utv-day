using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using MediatR;
using Database;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using System.Linq;
using FluentValidation;

namespace Features.Card
{
    public class Delete
    {
        public class Command : IRequest<Model>
        {
            public Guid Id { get; set; }
        }

        public class Validator : AbstractValidator<Command>
        {
            public Validator()
            {
                RuleFor(c => c.Id).NotEmpty();
            }
        }

        public class Model
        {
            public Guid Id { get; set; }
        }

        public class DeleteHandler : IRequestHandler<Command, Model>
        {
            private readonly ApplicationDbContext _dbContext;
            private readonly IMapper _mapper;

            public DeleteHandler(ApplicationDbContext dbContext, IMapper mapper)
            {
                _dbContext = dbContext;
                _mapper = mapper;
            }

            public async Task<Model> Handle(Command command, CancellationToken token)
            {
                var resultToDelete = await _dbContext.Cards.FirstOrDefaultAsync(c => c.Id == command.Id);

                if (resultToDelete == null)
                {
                    return new Model();
                }

                var result = _dbContext.Cards.Remove(resultToDelete);
                await _dbContext.SaveChangesAsync(token);

                return new Model() { Id = result.Entity.Id };
            }
        }
    }
}