namespace Features.Card
{
    using AutoMapper;
    using Core.Entities;

    public class MappingConfiguration : Profile
    {
        public MappingConfiguration()
        {
            CreateMap<Card, GetByDay.Model>();

            CreateMap<CreateOrUpdate.Command, Card>(MemberList.Source);
            CreateMap<Card, CreateOrUpdate.Model>();

            CreateMap<Vote.Command, CardVote>(MemberList.Source);
            CreateMap<CardVote, Vote.Model>();
        }
    }
}