using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using MediatR;
using Database;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using System.Linq;
using FluentValidation;

namespace Features.Card
{
    public class Unvote
    {
        public class Command : IRequest<bool>
        {
            public Guid Id { get; set; }
        }

        public class Validator : AbstractValidator<Command>
        {
            public Validator()
            {
                RuleFor(c => c.Id).NotEmpty();
            }
        }

        public class UnvoteHandler : IRequestHandler<Command, bool>
        {
            private readonly ApplicationDbContext _dbContext;
            private readonly IMapper _mapper;

            public UnvoteHandler(ApplicationDbContext dbContext, IMapper mapper)
            {
                _dbContext = dbContext;
                _mapper = mapper;
            }

            public async Task<bool> Handle(Command command, CancellationToken token)
            {
                var resultToDelete = await _dbContext.CardVotes.FirstOrDefaultAsync(cv => cv.Id == command.Id);

                if (resultToDelete == null)
                {
                    return false;
                }

                var result = _dbContext.CardVotes.Remove(resultToDelete);
                await _dbContext.SaveChangesAsync(token);
                return true;
            }
        }
    }
}