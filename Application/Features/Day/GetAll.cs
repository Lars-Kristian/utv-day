using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using MediatR;
using Database;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace Features.Day
{
    public class GetAll
    {
        public class Query : IRequest<List<Model>> { }

        public class Model
        {
            public Guid Id { get; set; }
            public DateTime Created { get; set; }
            public string Name { get; set; }
        }

        public class DayGetAllHandler : IRequestHandler<Query, List<Model>>
        {
            private readonly ApplicationDbContext _dbContext;
            private readonly IConfigurationProvider _mapper;

            public DayGetAllHandler(ApplicationDbContext dbContext, IConfigurationProvider  mapper)
            {
                _dbContext = dbContext;
                _mapper = mapper;
            }

            public async Task<List<Model>> Handle(Query request, CancellationToken token)
            {
                var result = await _dbContext.Days.ProjectTo<Model>(_mapper).ToListAsync(token);
                return result;
            }
        }
    }
}