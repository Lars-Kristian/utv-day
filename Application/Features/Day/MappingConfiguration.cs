namespace Features.Day
{
    using AutoMapper;
    using Core.Entities;

    public class MappingConfiguration : Profile
    {
        public MappingConfiguration()
        {
            CreateMap<Day, GetAll.Model>();

            CreateMap<Create.Command, Day>(MemberList.Source);
            CreateMap<Day, Create.Model>();
        }

    }
}