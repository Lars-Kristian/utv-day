using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using MediatR;
using Database;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using FluentValidation;

namespace Features.Day
{
    public class Create
    {
        public class Command : IRequest<Model> {
            public string Name { get; set; }
        }

        public class Validator : AbstractValidator<Command>
        {
            public Validator()
            {
                RuleFor(c => c.Name).NotEmpty();
            }
        }

        public class Model
        {
            public Guid Id { get; set; }
            public DateTime Created { get; set; }
            public string Name { get; set; }
        }

        public class CreateHandler : IRequestHandler<Command, Model>
        {
            private readonly ApplicationDbContext _dbContext;
            private readonly IMapper _mapper;

            public CreateHandler(ApplicationDbContext dbContext, IMapper  mapper)
            {
                _dbContext = dbContext;
                _mapper = mapper;
            }

            public async Task<Model> Handle(Command command, CancellationToken token)
            {   
                var data = _mapper.Map<Core.Entities.Day>(command);
                data.Id = new Guid();
                data.Created = DateTime.Now;

                var result = await _dbContext.Days.AddAsync(data, token);
                await _dbContext.SaveChangesAsync(token);                

                var model = _mapper.Map<Model>(result.Entity);
                return model;
            }
        }
    }
}