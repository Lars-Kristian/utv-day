import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginModule } from './component/login/login.module';
import { CommonModule } from '@angular/common';
import { CardsModule } from './service/cards/cards.module';
import { CardModule } from './component/card/card.module';
import { DayModule } from './service/day/day.module';
import { FormsModule } from '@angular/forms';
import { CardListModule } from './component/card-list/card-list.module';
import { ApplicationHttpClientModule } from './service/applicationHttpClient/applicationHttpClient.module';
import { DayEditorModule } from './component/day-editor/day-editor.module';
import { UserServiceModule } from './service/user/user.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    ApplicationHttpClientModule,
    UserServiceModule,
    DayEditorModule,
    LoginModule,
    CardModule,
    CardsModule,
    CardListModule,
    DayModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
