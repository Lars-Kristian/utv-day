import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";

import { LoginComponent } from './login.component';
import { UserServiceModule } from '../../service/user/user.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    UserServiceModule
  ],
  declarations: [LoginComponent],
  exports: [LoginComponent]
})
export class LoginModule { }


export { LoginComponent };