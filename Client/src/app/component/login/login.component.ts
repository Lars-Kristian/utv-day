import { Component } from '@angular/core';
import { UserService } from 'src/app/service/user/user.module';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  name: string = "";

  constructor(private userService: UserService) {}

  showLogin(): boolean{
    return !this.userService.hasUser()
  }

  updateName(){
    var name = this.userService.getUser();
    if(name){
      this.name = name;
    }
  }

  onChangeInput(event: KeyboardEvent){
    var name = (event.target as HTMLInputElement).value;
    this.userService.setUser(name);
    this.updateName();
  }
}