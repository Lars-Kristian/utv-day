import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ICard, CardsService } from 'src/app/service/cards/cards.module';
import { ICardVote } from 'src/app/service/cards/cards.interface';
import { UserService } from 'src/app/service/user/user.module';

@Component({
    selector: 'app-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

    @Input() card: ICard;

    editCard: ICard;
    edit: boolean = false;

    constructor(private cardsService: CardsService, private userService: UserService) {

    }
    ngOnInit(): void {
        if(this.cardIsTemprary()){
            this.enableEdit();
        }    
    }

    cardIsTemprary(): boolean {
        if (this.card.id.indexOf('tmp-') == 0) return true;
        return false;
    }

    editModeIsEnabled(): boolean {
        return this.edit;
    }

    showName(): boolean {
        if (!this.card.name || this.card.name.length <= 0) return false;
        return true;
    }

    showTechnology(): boolean {
        if (!this.card.technology || this.card.technology.length <= 0) return false;
        return true;
    }

    showComment(): boolean {
        if (!this.card.comment || this.card.comment.length <= 0) return false;
        return true;
    }

    showContact(): boolean {
        if (!this.card.contact || this.card.contact.length <= 0) return false;
        return true;
    }

    showSlack(): boolean {
        if (!this.card.slack || this.card.slack.length <= 0) return false;
        return true;
    }

    showVotes(): boolean {
        if (this.editModeIsEnabled()) return false;

        if (!this.card.votes || this.card.votes.length == 0) {
            return false;
        }

        return true;
    }

    userHasVoted(): boolean {
        var vote = this.cardsService.getVote(this.userService.getUser(), this.card);

        if (vote) {
            return true;
        }

        return false;
    }

    showVoteButton(): boolean {
        if (!this.userService.hasUser()) return false;
        if (this.userHasVoted()) return false;
        return true;
    }

    showUnvoteButton(): boolean {
        if (!this.userService.hasUser()) return false;
        if (this.userHasVoted()) return true;
        return false;
    }

    onClickVote() {
        this.cardsService.saveVote(this.card, this.userService.getUser());
    }

    onClickUnvote() {
        this.cardsService.removeVote(this.card, this.userService.getUser());
    }

    enableEdit() {
        this.editCard = JSON.parse(JSON.stringify(this.card));
        this.edit = true;
    }

    saveEdit() {
        this.card = JSON.parse(JSON.stringify(this.editCard));
        this.cardsService.saveCard(this.editCard);
        //TODO: wait for result of server, then update gui?
        this.edit = false;
    }

    showDeleteButton(): boolean {
        if (this.cardIsTemprary()) return false;
        return true;
    }

    deleteEdit() {
        this.cardsService.deleteCard(this.editCard);
        this.edit = false;
    }

    cancelEdit() {
        if(this.cardIsTemprary()){
            this.cardsService.deleteCardFromLocalState(this.editCard.id);
        }
        this.edit = false;
    }
}
