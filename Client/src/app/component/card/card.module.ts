import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";

import { CardsModule } from 'src/app/service/cards/cards.module';
import { CardComponent } from './card.component';
import { LoginModule } from '../login/login.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CardsModule,
    LoginModule
  ],
  declarations: [CardComponent],
  exports: [CardComponent]
})
export class CardModule { }
