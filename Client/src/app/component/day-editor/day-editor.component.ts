import { Component } from '@angular/core';
import { DayService } from 'src/app/service/day/day.module';

@Component({
  selector: 'app-day-editor',
  templateUrl: './day-editor.component.html',
  styleUrls: ['./day-editor.component.css']
})
export class DayEditorComponent {


  enabled = false;

  eventName = "";

  constructor(private dayService: DayService) { }

  enableComponent(){
    this.enabled = true;
  }

  disableComponent(){
    this.enabled = false;
  }

  showPopup():boolean {
    return this.enabled;
  }

  onChangeInput(event: KeyboardEvent){
    this.dayService.createEventDay(this.eventName);
  }
}
