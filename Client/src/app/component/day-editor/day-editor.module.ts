import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { DayEditorComponent } from './day-editor.component';
import { DayModule } from '../../service/day/day.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DayModule
  ],
  declarations: [DayEditorComponent],
  exports: [DayEditorComponent]

})
export class DayEditorModule { }

export { DayEditorComponent }