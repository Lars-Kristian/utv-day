import { Component } from '@angular/core';
import { CardsService, ICard } from 'src/app/service/cards/cards.module';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.css']
})
export class CardListComponent {

  cards: ICard[];
  cardsServiceChangeSubscription: Subscription;

  constructor(private cardsService: CardsService) {
    this.cardsServiceChangeSubscription = cardsService.change.subscribe(this.onChangeCardsService.bind(this));

  }

  onChangeCardsService(cards: ICard[]) {
    this.cards = cards;
  }

  showCards() {
    if (!this.cards || this.cards.length <= 0) {
      return false;
    }
    return true;
  }
}
