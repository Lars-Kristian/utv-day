import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardListComponent } from './card-list.component';
import { CardsModule } from 'src/app/service/cards/cards.module';
import { CardModule } from '../card/card.module';

@NgModule({
  declarations: [CardListComponent],
  exports: [CardListComponent],
  imports: [
    CommonModule,
    CardModule,
    CardsModule
  ]
})
export class CardListModule { }
