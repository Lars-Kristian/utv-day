import { Injectable, EventEmitter } from '@angular/core';
import { ICard } from './cards.module';
import { IdGenerator } from 'src/app/util/idGenerator';
import { ApplicationHttpClient } from '../applicationHttpClient/applicationHttpClient.module';
import { ICardVote } from './cards.interface';

@Injectable()
export class CardsService {
    cards: ICard[] = [];
    change: EventEmitter<ICard[]> = new EventEmitter();

    constructor(private applicationHttpClient: ApplicationHttpClient) { }

    fetchCards(dayId: string) {
        console.log('Fetching cards for dayId: ' + dayId);
        this.applicationHttpClient.get('card/byDay/' + dayId).subscribe((data: ICard[]) => {
            console.log('Ok: Fetching cards', data);
            this.cards = data;
            this.emitChangeEvent();
        }, (error) => {
            console.log('Error: Fetching cards', error);
        });
    }

    emitChangeEvent() {
        this.change.emit(this.cards);
    }

    newCard(dayId: string): ICard {
        var card: ICard = {
            id: 'tmp-' + IdGenerator.get(),
            dayId: dayId,
            name: 'New Card',
            technology: '',
            contact: '',
            slack: '',
            votes: []
        };

        this.cards.push(card);
        this.emitChangeEvent();
        return card;
    }

    insertCard(oldId: string, newCard: ICard) {
        var index = this.cards.findIndex(c => c.id == oldId);
        if (index >= 0) {
            this.cards.splice(index, 1, newCard);
        } else {
            this.cards.push(newCard);
        }
    }

    saveCard(card: ICard) {
        var cardCopy = <ICard>JSON.parse(JSON.stringify(card));

        if (cardCopy.id.indexOf('tmp-') != -1) {
            cardCopy.id = null;
        }

        console.log('Saving card', cardCopy);
        this.applicationHttpClient.post('card', cardCopy).subscribe(
            (result: ICard) => {
                console.log('Ok: Saving card', result);
                this.insertCard(card.id, result);
                this.emitChangeEvent();
            }, (error) => {
                console.log('Error: Saving card', error);
            }
        );
    }

    deleteCardFromLocalState(id: string) {
        var index = this.cards.findIndex(c => c.id == id);
        if (index <= -1) return;
        
        this.cards.splice(index, 1);
        
        this.emitChangeEvent();
    }

    deleteCard(card: ICard) {
        var command = {
            id: card.id
        };

        console.log('Deleting card');
        this.applicationHttpClient.delete('card', command).subscribe(
            (result: { id: string }) => {
                console.log('Ok: Deleting card', result);
                if (!result) return;
                this.deleteCardFromLocalState(result.id);
            }, (error) => {
                console.log('Error: Deleting card', error);
            }
        );
    }

    saveVote(card: ICard, nickname: string) {

        var cardVote = this.getVote(nickname, card);

        if (cardVote) {
            console.log('Vote found, unable to create new vote');
            return;
        }

        var vote: ICardVote = {
            cardId: card.id,
            nickname: nickname
        }

        console.log('Saving vote:', vote);
        this.applicationHttpClient.post('card/vote', vote).subscribe(
            (result: ICardVote) => {
                console.log('Ok: Saving vote', result);

                var card = this.getCard(result.cardId);

                if (!card.votes) { //Shame shame shame
                    card.votes = [];
                }

                card.votes.push(result);
                this.emitChangeEvent();

            }, (error) => {
                console.log('Error: Saving vote', error);
            }
        );
    }

    removeVote(card: ICard, nickname: string) {

        var cardVote = this.getVote(nickname, card);

        if (!cardVote) {
            console.log('Vote not found, unable to remove vote');
            return;
        }

        console.log('Removing vote', cardVote);
        this.applicationHttpClient.post('card/unvote', cardVote).subscribe(
            (result) => {
                console.log('Ok: Removing vote', result);

                var card = this.getCard(cardVote.cardId);
                var voteIndex = card.votes.findIndex(cv => cv.id == cardVote.id);

                if (voteIndex >= 0) {
                    card.votes.splice(voteIndex, 1);
                }

                this.emitChangeEvent();

            }, (error) => {
                console.log('Error: Removing vote', error);
            }
        );
    }

    getCard(cardId: string): ICard {
        return this.cards.find(c => c.id == cardId);
    }

    getVote(nickname: string, card: ICard): ICardVote {
        if (!nickname || !card || !card.votes || card.votes.length <= 0) return;

        var cardVote = card.votes.find((vote: ICardVote) => {
            if (vote.nickname == nickname) {
                return true;
            }
            return false;
        });

        return cardVote;
    }
}
