import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ICard } from './cards.interface';
import { CardsService } from './cards.service';
import { ApplicationHttpClientModule } from '../applicationHttpClient/applicationHttpClient.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ApplicationHttpClientModule
  ],
  providers: [CardsService]
})
export class CardsModule { }
export { ICard, CardsService }
