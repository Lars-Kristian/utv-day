export interface ICard {
    id: string;
    dayId: string;
    name: string;
    technology?: string;
    contact?: string;
    slack?: string;
    comment?: string;
    votes: ICardVote[];
}

export interface ICardVote {
    id?: string;
    cardId: string;
    nickname: string;
}