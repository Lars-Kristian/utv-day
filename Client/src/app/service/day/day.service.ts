import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ApplicationHttpClient } from '../applicationHttpClient/applicationHttpClient.module';

export interface IDay {
  id?: string,
  name: string
};

export interface IDayServiceState {
  selectedDay: IDay;
  dayOptions: IDay[];
}

@Injectable()
export class DayService {

  state: IDayServiceState = {
    selectedDay: null,
    dayOptions: []
  };

  change: BehaviorSubject<IDayServiceState> = new BehaviorSubject(null);
  
  constructor(private applicationHttpClient: ApplicationHttpClient) { }

  private emitChangeEvent(){
    this.change.next(JSON.parse(JSON.stringify(this.state)));
  }

  createEventDay(name: string){
    if(!name || name.trim() == '') return;

    var eventDay: IDay = { name: name };

    console.log('Creating event day', );
    this.applicationHttpClient.post('day', eventDay).subscribe(
      this.postCreateEventDayHandler.bind(this),
      this.postCreateEventDayErrorHandler.bind(this)
    );
  }

  postCreateEventDayHandler(data: IDay){
    console.log('Ok: Creating event day', data);
    this.fetchDayOptions();
  }

  postCreateEventDayErrorHandler(error: any){
    console.log('Error: Creating event day', error);
  }

  fetchDayOptions() {
    console.log('Fetching event day options');
    this.applicationHttpClient.get('day/all').subscribe(
      this.fetchDayOptionsHandler.bind(this),
      this.fetchDayOptionsErrorHandler.bind(this)
    );
  }

  fetchDayOptionsHandler(data: IDay[]){
    console.log('Ok: Fetching event day options', data);
    this.state.dayOptions = data;
    this.state.selectedDay = this.state.dayOptions[0];
    this.emitChangeEvent();
  }

  fetchDayOptionsErrorHandler(error: any){
    console.log('Error: Fetching event day options', error);
  }


  setSelectedDay(day: IDay) {
    this.state.selectedDay = day;
    this.emitChangeEvent();
  }
}
