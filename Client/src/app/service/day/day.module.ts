import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DayService, IDay, IDayServiceState } from './day.service';
import { ApplicationHttpClientModule } from '../applicationHttpClient/applicationHttpClient.module';

@NgModule({
  declarations: [],
  providers: [DayService],
  imports: [
    CommonModule,
    ApplicationHttpClientModule
  ]
})
export class DayModule { }

export { DayService, IDay, IDayServiceState };