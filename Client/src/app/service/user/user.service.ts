import { Injectable } from '@angular/core';

export class UserService {
  private nickname: string = "";

  constructor() {
    this.loadUser();
  }

  loadUser() {
    var nickname = localStorage.getItem('user');
    if (nickname != null && nickname != "") {
      this.nickname = nickname
    }
  }

  getUser(): string {
    return this.nickname;
  }

  setUser(nickname: string) {
    this.nickname = nickname.toUpperCase();
    localStorage.setItem('user', this.nickname);
  }

  hasUser(): boolean {
    if (!this.nickname || this.nickname.trim() == "") return false;
    return true;
  }
}
