import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicationHttpClient } from './applicationHttpClient.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [],
  providers: [ApplicationHttpClient],
  imports: [
    CommonModule,
    HttpClientModule
  ]
})
export class ApplicationHttpClientModule { }

export { ApplicationHttpClient };