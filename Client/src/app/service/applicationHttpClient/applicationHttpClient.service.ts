import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ApplicationHttpClient {

  private apiUrl: string;

  constructor(private http: HttpClient) { 
    this.apiUrl = location.protocol + "//" + location.hostname + ":" + location.port + "/api/";
  }

  get(urlPath: string): Observable<Object>{
    var url = this.apiUrl + urlPath;
    return this.http.get(url);
  }

  post(urlPath: string, data: object): Observable<Object>{
    var url = this.apiUrl + urlPath;
    return this.http.post(url, data);
  }

  delete(urlPath: string, data: object): Observable<Object>{
    var url = this.apiUrl + urlPath;
    return this.http.request('delete', url, {body: data})
  }
}
