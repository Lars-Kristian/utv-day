import { Component, OnInit, ViewChild } from '@angular/core';
import { CardsService } from './service/cards/cards.module';
import { DayService, IDay, IDayServiceState } from './service/day/day.module';
import { Subscription } from 'rxjs';
import { ApplicationHttpClient } from './service/applicationHttpClient/applicationHttpClient.module';
import { DayEditorComponent } from './component/day-editor/day-editor.module';
import { UserService } from './service/user/user.module';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  private dayServiceChangeSub: Subscription;
  dayOptions: IDay[];
  selectedDay: IDay;

  @ViewChild('dayEditor') dayEditorComponent: DayEditorComponent;

  constructor(private applicationHttpClient: ApplicationHttpClient,
              private userService: UserService,
              private cardsService: CardsService,
              private daysService: DayService) {
                
    window['app'] = this;

    this.dayServiceChangeSub = daysService.change.subscribe(this.dayServiceChangeHandler.bind(this));
  }

  ngOnInit(): void {
    this.daysService.fetchDayOptions();
  }

  dayServiceChangeHandler(state: IDayServiceState) {
    if (!state) return;

    if(!state.dayOptions || state.dayOptions.length == 0){
      this.dayEditorComponent.enableComponent();
    }else{
      this.dayEditorComponent.disableComponent();
    }

    if (!state.selectedDay) {
      this.daysService.setSelectedDay(state.dayOptions[0]);
      return;
    }

    this.dayOptions = state.dayOptions;
    this.selectedDay = state.selectedDay;

    this.cardsService.fetchCards(this.selectedDay.id);
  }

  showDaySelect() {
    if (!this.dayOptions || this.dayOptions.length <= 0) return false;
    return true;
  }

  dayComparer(a: IDay, b: IDay) {
    if (!a || !b) return false;
    return a.id == b.id;
  }

  onChangeSelectedDay(day: IDay) {
    this.daysService.setSelectedDay(day);
  }

  showUser() {
    return this.userService.hasUser();
  }

  onClickAddCard() {
    var card = this.cardsService.newCard(this.selectedDay.id);

    setTimeout(() => {
      var element = document.getElementById(card.id);

      if (element) {
        element.focus();
      }
    }, 0);
  }
}
