using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DayController : ControllerBase
    {
        private readonly IMediator _mediator;

        public DayController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("all")]
        public async Task<ActionResult> Get()
        {
            try
            {
                var result = await _mediator.Send(new Features.Day.GetAll.Query());
                return Ok(result);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost()]
        public async Task<ActionResult> Create([FromBody]Features.Day.Create.Command command)
        {
            try
            {
                var result = await _mediator.Send(command);
                return Ok(result);
            }
            catch (ValidationException ve)
            {
                return BadRequest(ve);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}