using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CardController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CardController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("byDay/{dayId}")]
        public async Task<ActionResult> GetByDay(Guid dayId)
        {
            try
            {
                var result = await _mediator.Send(new Features.Card.GetByDay.Query() { DayId = dayId });
                return Ok(result);
            }
            catch (ValidationException ve)
            {
                return BadRequest(ve);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost()]
        public async Task<ActionResult> Create([FromBody]Features.Card.CreateOrUpdate.Command command)
        {
            try
            {
                var result = await _mediator.Send(command);
                return Ok(result);
            }
            catch (ValidationException ve)
            {
                return BadRequest(ve);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpDelete()]
        public async Task<ActionResult> Delete([FromBody]Features.Card.Delete.Command command)
        {
            try
            {
                var result = await _mediator.Send(command);
                return Ok(result);
            }
            catch (ValidationException ve)
            {
                return BadRequest(ve);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost("vote")]
        public async Task<ActionResult> Vote([FromBody] Features.Card.Vote.Command command)
        {
            try
            {
                var result = await _mediator.Send(command);
                return Ok(result);
            }
            catch (ValidationException ve)
            {
                return BadRequest(ve);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost("unvote")]
        public async Task<ActionResult> Unvote([FromBody] Features.Card.Unvote.Command command)
        {
            try
            {
                var result = await _mediator.Send(command);
                return Ok(result);
            }
            catch (ValidationException ve)
            {
                return BadRequest(ve);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}