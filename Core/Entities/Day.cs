using System;
using System.Collections.Generic;

namespace Core.Entities
{
    public class Day
    {
        public Guid Id { get; set; }
        public DateTime Created { get; set; }
        public string Name { get; set; }
        
        public List<Card> Cards { get; set; }
    }
}