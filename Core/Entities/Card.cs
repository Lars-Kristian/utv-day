using System;
using System.Collections.Generic;

namespace Core.Entities
{
    public class Card
    {
        public Guid Id { get; set; }
        public DateTime Created { get; set; }
        public string Name { get; set; }
        public string Technology { get; set; }
        public string Slack { get; set; }
        public string Contact { get; set; }
        public string Comment { get; set; }
        public List<CardVote> Votes { get; set; }

        public Guid DayId { get; set; }
        public Day Day { get; set; }
    }
}