using System;

namespace Core.Entities
{
    public class CardVote
    {
        public Guid Id { get; set; }
        public Guid CardId { get; set; }
        public Card Card { get; set; }
        public string Nickname { get; set; }
    }
}