FROM microsoft/dotnet:2.1-sdk AS build

WORKDIR /src

COPY utv-day.sln ./
COPY Core/Core.csproj ./Core/
COPY Database/Database.csproj ./Database/
COPY Application/Application.csproj ./Application/
COPY Server/Server.csproj ./Server/

RUN dotnet restore

COPY ./Core ./Core
COPY ./Database ./Database
COPY ./Application ./Application
COPY ./Server ./Server

WORKDIR /src/Server

RUN dotnet publish Server.csproj -c Release -o /app

FROM microsoft/dotnet:2.1-aspnetcore-runtime AS final

ENV TZ=Europe/Oslo
ENV RUNNING_IN_CONTAINER=true
ENV DATABASE=postgres

WORKDIR /app
COPY --from=build /app .
ENTRYPOINT ["dotnet", "Server.dll"]
