using Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace Database.Configurations
{
    public class CardConfiguration
    {
        public void Configure(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Card>()
                .Property(e => e.Id)
                .IsRequired();

            modelBuilder.Entity<Card>()
                .Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(255);

            modelBuilder.Entity<Card>()
                .HasOne(c => c.Day)
                .WithMany(d => d.Cards)
                .HasForeignKey(c => c.DayId);
        }
    }
}