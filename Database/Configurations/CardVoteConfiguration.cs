using Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace Database.Configurations
{
    public class CardVoteConfiguration
    {
        public void Configure(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CardVote>()
                .Property(e => e.Id)
                .IsRequired();

            modelBuilder.Entity<CardVote>()
                .Property(e => e.CardId)
                .IsRequired();

            modelBuilder.Entity<CardVote>()
                .Property(e => e.Nickname)
                .IsRequired()
                .HasMaxLength(255);

            modelBuilder.Entity<CardVote>()
                .HasOne(cv => cv.Card)
                .WithMany(c => c.Votes)
                .HasForeignKey(cv => cv.CardId);
        }
    }
}