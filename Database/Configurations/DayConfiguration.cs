using Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace Database.Configurations
{
    public class DayConfiguration
    {
        public void Configure(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Day>()
                .Property(e => e.Id)
                .IsRequired();

            modelBuilder.Entity<Day>()
                .Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(255);
        }
    }
}