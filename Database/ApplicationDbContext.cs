using System;
using System.Collections.Generic;
using Core.Entities;
using Database.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Database
{
    public class ApplicationDbContext : DbContext
    {
        private readonly IConfiguration _configuration;
        
        public ApplicationDbContext(
            DbContextOptions<ApplicationDbContext> options, 
            IConfiguration configuration) 
            : base(options)
        {
            _configuration = configuration;
        }
        
        public DbSet<Day> Days { get; set; }
        public DbSet<Card> Cards { get; set; }
        public DbSet<CardVote> CardVotes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = _configuration.GetConnectionString("ApplicationDbConnection");
            optionsBuilder.UseNpgsql(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            new DayConfiguration().Configure(modelBuilder);
            new CardConfiguration().Configure(modelBuilder);
            new CardVoteConfiguration().Configure(modelBuilder);
        }
    }
}